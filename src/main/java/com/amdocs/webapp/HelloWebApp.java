package com.amdocs.webapp;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSessionBindingListener;

public class HelloWebApp extends HttpServlet {
	private static final long serialVersionUID = 1L;
	  protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
	         throws ServletException, IOException {
	      resp.setContentType("text/plain");
	      resp.getWriter().write("Hi Girish Kutala Maven Web Project Example.");
	      int i=10,j;
	      if (i==100)
	      {
			System.out.print(i);
	      }
	   }
}
